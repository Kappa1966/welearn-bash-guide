Comandi:
----------------------
cp
----------------------
Copies a file from one location to other.

cp filename1 filename2

Where filename1 is the source path to the file and filename2 is the destination
 path to the file.
-----------------------
diff
-----------------------
Compares files, and lists their differences.

diff filename1 filename2
-----------------------
file
-----------------------
Determine file type.

file filename

Example:

$ file index.html
 
index.html: HTML document, ASCII text

-----------------------
gzip
-----------------------
Compresses files.

gzip filename

-----------------------
gunzip
-----------------------
Un-compresses files compressed by gzip.

-----------------------
gunzip filename
-----------------------
find

Find files in directory
find directory options pattern

Example:
$ find . -name README.md
$ find /home/user1 -name '*.png'
-----------------------
ls
-----------------------
Lists your files.
-----------------------
mv
-----------------------
Moves a file from one location to other.

mv filename1 filename2

Where filename1 is the source path to the file and filename2 is the destination path to the file.

Also it can be used for rename a file.

mv old_name new_name
------------------------




